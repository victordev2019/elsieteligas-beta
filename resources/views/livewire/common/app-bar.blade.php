<header class="bg-primary-800 text-primary-200">
    <div class="container flex items-center h-20 justify-between">
        {{-- logo --}}
        <div>
            <a class="flex items-center" href="/">
                <x-application-mark class="block h-9 w-auto" />
                <span class="ml-2 text-primary-100 text-2xl">elsieteligas</span>
            </a>
        </div>
        {{-- right controls --}}
        <div class="flex">

            @auth
                <!-- Settings Dropdown -->
                <div class="ml-3 relative">
                    <x-dropdown align="right" width="48">
                        <x-slot name="trigger">
                            @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                                <button
                                    class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition">
                                    <img class="h-8 w-8 rounded-full object-cover"
                                        src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                                </button>
                            @else
                                <span class="inline-flex rounded-md">
                                    <button type="button"
                                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition">
                                        {{ Auth::user()->name }}

                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none"
                                            viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                        </svg>
                                    </button>
                                </span>
                            @endif
                        </x-slot>

                        <x-slot name="content">
                            <!-- Account Management -->
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Manage Account') }}
                            </div>

                            <x-dropdown-link href="{{ route('profile.show') }}">
                                {{ __('Profile') }}
                            </x-dropdown-link>

                            @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                                <x-dropdown-link href="{{ route('api-tokens.index') }}">
                                    {{ __('API Tokens') }}
                                </x-dropdown-link>
                            @endif

                            <div class="border-t border-gray-100"></div>

                            <!-- Authentication -->
                            <form method="POST" action="{{ route('logout') }}" x-data>
                                @csrf

                                <x-dropdown-link href="{{ route('logout') }}" @click.prevent="$root.submit();">
                                    {{ __('Log Out') }}
                                </x-dropdown-link>
                            </form>
                        </x-slot>
                    </x-dropdown>
                </div>
            @else
                <button class="bg-primary-700 px-3 rounded-md hover:bg-primary-900 flex items-center w-10 mr-2"><i
                        class="fa-solid fa-magnifying-glass"></i>
                </button>
                <a href="{{ route('login') }}"
                    class="bg-primary-700 w-10 block text-center py-2 lg:flex lg:w-auto lg:items-center lg:px-4 rounded-md hover:bg-primary-900"><i
                        class="fa-solid fa-user"></i><span
                        class="hidden lg:block ml-2 uppercase text-sm pt-1 font-bold">iniciar</span>
                </a>

                {{-- hamburger --}}
                {{-- <a href="/" class="w-10 ml-2 bg-primary-700 p-2 hover:bg-primary-900 rounded-md">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{ 'hidden': open, 'inline-flex': !open }" class="inline-flex" stroke-linecap="round"
                            stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{ 'hidden': !open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                            stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </a> --}}
                {{-- test hamburguer --}}
                <!-- Settings Dropdown -->
                <div wire:ignore.self class="ml-2 relative">
                    <x-dropdown align="right" width="w-80">
                        <x-slot name="trigger">
                            <button
                                class="bg-primary-700 px-3 rounded-md hover:bg-primary-900 flex items-center w-10 mr-2 h-10"><i
                                    :class="!open ? 'fa-solid fa-gear' : 'fa-solid fa-xmark text-2xl ml-[1.13px]'"
                                    x-transition.duration.1000ms></i></button>
                        </x-slot>

                        <x-slot name="content">
                            <!-- Account Management -->
                            @livewire('common.setting', ['municipality' => $municipality])
                        </x-slot>
                    </x-dropdown>
                </div>
            @endauth
        </div>
    </div>
    @push('script')
        <script>
            document.addEventListener('livewire:load', function() {
                // Get the value of the "count" property
                @this.muni = localStorage.getItem("municipality");
                // @this.test();
                // Run a callback when an event ("foo") is emitted from this component
                // @this.on('render', () => {})
            })
        </script>
    @endpush
</header>
