<div class="m-6 text-gray-700 dark:bg-primary-900 dark:text-gray-100">
    <h1 class="uppercase mb-4">Configuración</h1>
    <!-- This is an example component -->
    <div class="max-w-2xl mx-auto mb-2">
        <label class="block mb-1 text-sm font-medium text-gray-900 dark:text-gray-400" for="">Seleccione un
            Departamento</label>
        <select wire:model='department_id' class="combo-box" id="exampleFormControlSelect1">
            <option value="" selected>Elige un departamento...</option>
            @foreach ($departments as $department)
                <option value="{{ $department->id }}">{{ $department->name }}</option>
            @endforeach
        </select>
        @error('department_id')
            <span class="text-red-500 text-sm">{{ $message }}</span>
        @enderror
    </div>
    {{-- <p>{{ $department_id }}</p> --}}
    <div class="fmax-w-2xl mx-auto mb-2">
        <label class="block mb-1 text-sm font-medium text-gray-900 dark:text-gray-400" for="">Seleccione una
            Provincia</label>
        <select wire:model='province_id' class="combo-box" id="exampleFormControlSelect1">
            <option value="" selected>Elige una provincia...</option>
            @foreach ($provinces as $province)
                <option value="{{ $province->id }}">{{ $province->name }}</option>
            @endforeach
        </select>
        @error('province_id')
            <span class="text-red-500 text-sm">{{ $message }}</span>
        @enderror
    </div>
    {{-- <p>{{ $province_id }}</p> --}}
    <div class="max-w-2xl mx-auto mb-2">
        <label class="block mb-1 text-sm font-medium text-gray-900 dark:text-gray-400" for="">Seleccione un
            Municipio</label>
        <select wire:model='municipality_id' class="combo-box" id="exampleFormControlSelect1">
            <option value="" selected>Elige un municipio...</option>
            @foreach ($municipalities as $municipality)
                <option value="{{ $municipality->id }}">{{ $municipality->name }}</option>
            @endforeach
        </select>
        @error('municipality_id')
            <span class="text-red-500 text-sm">{{ $message }}</span>
        @enderror
    </div>
    {{-- <p>{{ $municipality_id }}</p> --}}
    <div class="my-4 flex justify-end">
        {{-- <x-button wire:click='updateSettingLocation' @click='open=!open'>Cambiar</x-button> --}}
        <x-button wire:click='updateSettingLocation'>Cambiar</x-button>
    </div>
    <div x-data class="flex items-center justify-between">
        <label class="block mb-1 text-md font-medium text-gray-900 dark:text-gray-400" for="">Modo
            Oscuro</label>
        <button @click="$store.darkMode.toggle()"><i class="fa-solid fa-circle-half-stroke text-2xl"></i></button>

        {{-- <p class="m-2 p-2">Drak Mode: <strong x-data x-text='$store.darkMode.on'></strong></p> --}}
    </div>
    @push('script')
        <script>
            // CARGAR VALOR LOCALSTORAGE
            document.addEventListener('livewire:load', function() {
                @this.municipality_id = localStorage.getItem("municipality");
                @this.getPropeties();
            })
            // CAMBIAR LOLASTORAGE ID
            window.addEventListener('update-setting', munId => {
                localStorage.setItem("municipality", event.detail.munId);
                @this.getPropeties();
                this.open = !this.open;
                // console.log(open);
                // console.log('listener ok :)' + event.detail.munId);
            })
        </script>
    @endpush
</div>
