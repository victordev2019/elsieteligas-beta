<div class="dark:text-white my-4">
    <div>
        <p class="uppercase text-lg">{{ $department_name }} / {{ $province_name }} / {{ $municipality_name }}</p>
    </div>
    <section class="bg-white dark:bg-primary-800 rounded-md p-4">
        @foreach ($leagues as $league)
            <x-home.collapse-item title="{{ $league->name }}" count="{{ $league->tournaments->count() }}"
                color="{{ $league->color }}">
                @foreach ($league->tournaments as $tournament)
                    <li class="my-2">
                        {{ $tournament->sport->name }}-{{ $tournament->name }}-{{ $tournament->category }}-{{ $tournament->gender }}
                    </li>
                @endforeach
            </x-home.collapse-item>
        @endforeach
    </section>
    {{-- <x-jet-danger-button>test</x-jet-danger-button>
    <x-button-vat /> --}}
    <p class="my-8">{{ $department_name }}</p>
    @push('script')
        <script>
            document.addEventListener('livewire:load', function() {
                @this.muni = localStorage.getItem("municipality");
                @this.getPropeties();
            })
            window.addEventListener('update-muni', () => {
                @this.muni = localStorage.getItem("municipality");
                @this.getPropeties();
                // localStorage.setItem("municipality", event.detail.munId);
                // @this.getPropeties();
                console.log('actualización mun_id ok :)');
            })
        </script>
    @endpush
</div>
