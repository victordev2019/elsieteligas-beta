@props(['title', 'count', 'color' => '#f3f'])
<div x-data="{ open: false }" class="mt-2">
    {{-- HEADER --}}
    <div x-on:click="open=!open"
        class="flex justify-between items-center bg-slate-200 dark:bg-primary-700 cursor-pointer px-3 py-1 uppercase rounded-md">
        <div>
            <p class="text-gray-500 dark:text-white font-bold text-sm select-none"><i
                    class="fa-regular fa-star mr-2"></i><i class="fa-solid fa-trophy mr-2"
                    style="color: {{ $color }}"></i>{{ $title }}
            </p>
        </div>
        <div class="flex items-center select-none text-gray-400">
            <p class="text-xs lowercase mr-2">({{ $count }}) competiciones</p><i
                :class="!open ? 'rotate-0' : 'rotate-180'"
                class="fa-solid fa-chevron-down text-sm transition-transform duration-500 rotate-0"></i>
        </div>
    </div>
    {{-- CONTENT --}}
    <div x-show="open" x-transition.duration.500ms class="">
        <ul class="text-gray-500 dark:text-gray-200 font-semibold capitalize ml-4">
            {{ $slot }}
        </ul>
    </div>
</div>
