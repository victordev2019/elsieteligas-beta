window.addEventListener("load", (e) => {
    if (!localStorage.getItem("dark")) {
        localStorage.setItem(
            "dark",
            window.matchMedia("(prefers-color-scheme: dark)").matches
        );
    }
    if (!localStorage.getItem("municipality")) {
        localStorage.setItem("municipality", 173);
    }
    if (JSON.parse(localStorage.getItem("dark")) === true) {
        console.log("dark");
        document.documentElement.classList.add("dark");
    } else {
        console.log("no dark");
        document.documentElement.classList.remove("dark");
    }
});
