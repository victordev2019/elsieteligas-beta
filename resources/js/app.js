import "./bootstrap";
import "./main";

import Alpine from "alpinejs";
import focus from "@alpinejs/focus";

let dark = true;
Alpine.store("darkMode", {
    on: JSON.parse(localStorage.getItem("dark")) === true,
    toggle() {
        console.log(localStorage.getItem("dark"));
        this.on = !this.on;
        if (this.on) {
            document.documentElement.classList.add("dark");
            localStorage.setItem("dark", true);
        } else {
            document.documentElement.classList.remove("dark");
            localStorage.setItem("dark", false);
        }
    },
});

window.Alpine = Alpine;

Alpine.plugin(focus);

Alpine.start();
