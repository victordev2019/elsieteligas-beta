<?php

namespace App\Http\Livewire\Common;

use Livewire\Component;

class AppBar extends Component
{
    public $municipalities;
    public $provinces, $departments;
    public $department_id, $province_id, $municipality_id;
    public $municipality;
    public $muni;
    public function render()
    {
        return view('livewire.common.app-bar');
    }
}
