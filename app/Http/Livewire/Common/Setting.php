<?php

namespace App\Http\Livewire\Common;

use App\Models\Department;
use App\Models\Municipality;
use App\Models\Province;
use Livewire\Component;

class Setting extends Component
{
    protected $listeners = ['updateSettingLocation'];
    protected $rules = [
        'department_id' => 'required',
        'province_id' => 'required',
        'municipality_id' => 'required',
    ];
    public $municipalities = [], $provinces = [], $departments = [];
    public $municipality_id, $province_id, $department_id;
    public function render()
    {
        return view('livewire.common.setting');
    }
    public function updatedDepartmentId()
    {
        $this->provinces = Province::where('department_id', $this->department_id)->get();
        $this->municipalities = [];
        $this->province_id = "";
        $this->municipality_id = "";
    }
    public function updatedProvinceId()
    {
        $this->municipalities = Municipality::where('province_id', $this->province_id)->get();
        $this->municipality_id = "";
    }
    // public function updatedMunicipalityId()
    // {
    //     dd('muni ok!!!');
    //     // $this->emit('storageIdUpdate', $this->municipality_id);
    // }
    public function updateSettingLocation()
    {
        $this->validate($this->rules);
        $this->dispatchBrowserEvent('update-setting', ['munId' => $this->municipality_id]);
        $this->emit('load-leagues');

        // redirect()->route('home');
    }
    public function getPropeties()
    {
        $this->departments = Department::all();
        $municipality = Municipality::find($this->municipality_id);
        $this->province_id = $municipality->province_id;
        $province = Province::find($this->province_id);
        $this->department_id = $province->department_id;

        $this->provinces = Province::where('department_id', $this->department_id)->get();
        $this->municipalities = Municipality::where('province_id', $this->province_id)->get();
        // try {
        // } catch (Exception $e) {
        //     redirect()->route('home');
        //     echo $e->getMessage();
        // }
        // $this->municipality = Municipality::find($this->muni);
        // $this->province = $this->municipality->province;
        // $this->department = $this->municipality->province->department;
        // $this->province_name = $this->province->name;
        // $this->department_name = $this->department->name;

        // dd($this->province);
        // return view('livewire.web.league.league-tournament');
    }
}
