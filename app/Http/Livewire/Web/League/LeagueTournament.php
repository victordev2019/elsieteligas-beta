<?php

namespace App\Http\Livewire\Web\League;

use App\Models\League;
use App\Models\Municipality;
use Exception;
use Livewire\Component;

class LeagueTournament extends Component
{
    protected $listeners = ['load-leagues' => 'updateMunicipality'];
    public $muni;
    public $municipality, $province, $department;
    public $province_name, $department_name, $municipality_name;
    public function render()
    {
        // $this->municipality = Municipality::where('id', $this->muni)->get();
        $leagues = League::where('municipality_id', $this->muni)->get();
        return view('livewire.web.league.league-tournament', compact('leagues'));
    }
    public function getPropeties()
    {
        try {
            //code...
            $this->municipality = Municipality::find($this->muni);
            $this->municipality_name = $this->municipality->name;
            $this->province = $this->municipality->province;
            $this->department = $this->municipality->province->department;
            $this->province_name = $this->province->name;
            $this->department_name = $this->department->name;
        } catch (Exception $e) {
            redirect()->route('home');
            echo $e->getMessage();
        }
        // dd($this->province);
        // return view('livewire.web.league.league-tournament');
    }
    public function updateMunicipality()
    {
        $this->dispatchBrowserEvent('update-muni');
    }
}
