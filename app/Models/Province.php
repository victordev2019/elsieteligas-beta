<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    // RELATIONS
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }
}
