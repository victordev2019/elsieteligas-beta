<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    // VAT CODE
    // RELATIONS
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }
    public function tournaments()
    {
        return $this->hasMany(Tournament::class);
    }
}
